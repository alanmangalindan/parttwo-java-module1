package ictgradschool.industry.introtojava;

import ictgradschool.industry.introtojava.simplemaths.TestSimpleMaths;
import ictgradschool.industry.introtojava.sortnumbers.TestSortNumbers;
import ictgradschool.industry.introtojava.elapsedtime.TestElapsedTime;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestSimpleMaths.class,
        TestSortNumbers.class,
        TestElapsedTime.class
})
public class TestSuite { }
