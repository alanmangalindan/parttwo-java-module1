package ictgradschool.industry.introtojava.mysecondprogram;

public class MySecondProgram {
    public void start() {
        System.out.println("Hello World from MySecondProgram");
    }

    private void printRowOfAmpersands (int howMany) {
        for (int i = 0; i < howMany; i ++) {
            System.out.print("&");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        MySecondProgram p = new MySecondProgram();
        p.start();

        System.out.println((int)2.9 * 4.5);
        System.out.println(Math.max(5,60) % Math.min(12,7));
        System.out.println(0.2 * 3 / 2 + 3 / 2 * 3.2);

        int a = 7;
        int b = 1;
        int c = a + 2;
        a = b;
        b = c;
        c = c + 1;
        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);

        // industry_lab_03: Exercise Four
        int number = 5;
        while (number < 15) {
            System.out.print(3 * number + " ");
            number += 4;
        }
        System.out.println();
        System.out.println("Number is now: " + number);

        p.printRowOfAmpersands(8);


//        System​.​out​.​println​((​int​)​2.9​ ​*​ ​4.5​);
//        System​.​out​.​println​((​int​)​2.9​ ​*​ ​4.5​);
//        System​.​out​.​println​(​Math​.​max​(​5​,​60​)​ ​%​ ​Math​.​min​(​12​,​7​));
//        System​.​out​.​println​(​0.2​ ​*​ ​3​ ​/​ ​2​ ​+​ ​3​ ​/​ ​2​ ​*​ ​3.2​);
    }
}
